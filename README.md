# Purescript Web Extensions

Well, it's a start. Actually, turns out that it's *yet another start*. https://github.com/bodil/purescript-chrome-api

Well, Chrome and Firefox api's are different (promises vs callbacks).

## Usage

Read the files and then post issues when you're confused. Because I know there isn't enough documentation in there.

### Worth noting

Uses purescript-promises package. I recommend just downloading it from the github page, as it isn't in psc-package manager.

## Examples

I've used this [here](https://gitlab.com/losnappas/multiple-windows-single-session).

I didn't use Aff because I didn't know how to intertwine it with the event listeners. I did try, and that's why the Aff folder is here.

